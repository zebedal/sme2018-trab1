import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaskProvider } from '../../providers/task/task';
import { AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-add-task',
  templateUrl: 'add-task.html',
})

export class AddTaskPage {

  formgroup:FormGroup;
  name:AbstractControl;
  year:AbstractControl;
  genre:AbstractControl;

  public item:string;
  public quantity:number;
  public description:string;
  public select:string;
  public image_name:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public provider: TaskProvider,
     public alertCtrl: AlertController,public formbuilder:FormBuilder) {

       this.formgroup = formbuilder.group({
         name:['',Validators.compose([Validators.required,Validators.minLength(6)])],
         year:['',Validators.required],
         genre:['',Validators.compose([Validators.required,Validators.minLength(6)])],
         select:['',Validators.required],
       });

       this.name = this.formgroup.controls['name'];
       this.year = this.formgroup.controls['year'];
       this.genre = this.formgroup.controls['genre'];
  }

  addTask(){

    let image_source = "../../assets/imgs/"
    let full_path =image_source+this.image_name;

  /* Validava se o formulário estava em branco

   if(!this.formgroup.valid){

     console.log("form is empty");
     let alert = this.alertCtrl.create({
       title:'Form error',
       subTitle: 'The form cannot be empty, please fill all required fields',
       buttons: ['Dismiss']});
     alert.present();
     return null;
   }*/


   this.provider.add(this.item, this.quantity, this.description,this.select,full_path);

    //go back to previous screen
    if(this.navCtrl.canGoBack()){
      let alert = this.alertCtrl.create({
        title:'Item',
        subTitle: 'The item:&nbsp'+this.item+'&nbsphas been inserted successfully',
        buttons: ['Dismiss']});
      alert.present();
      this.navCtrl.pop();
    }

  }//end of addTask function

  fileEvent(fileInput: HTMLInputEvent){
    let file = fileInput.target.files[0];
    let fileName = file.name;
    this.image_name = fileName;
}


}
interface HTMLInputEvent extends Event {
    target: HTMLInputElement & EventTarget;
}

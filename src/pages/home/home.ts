import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AddTaskPage } from '../add-task/add-task';
import { TaskProvider } from '../../providers/task/task';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public tasks: string[];



  constructor(public navCtrl: NavController, public provider: TaskProvider){
    this.tasks = this.provider.tasks
  }

 deleteTask(task: string){
   this.provider.delete(task);
 }

  addTask(){
    this.navCtrl.push(AddTaskPage);

  }

}

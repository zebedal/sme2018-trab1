
import { Injectable } from '@angular/core';


@Injectable()
export class TaskProvider {

  constructor() {

    this.init();
  }

  public tasks:any[];


  init() {

      this.tasks = [{name:"Rabanetes", src:"../../assets/imgs/vegetais.jpg",year:1,genre:"Rabanetes frescos",type:"Units"},
      {name:"Carne Vitela", src:"../../assets/imgs/carne.jpg",year:500,genre:"Fatias finas de carne seleccionada",type:"Grams"},
      {name:"Robalos", src:"../../assets/imgs/peixe.jpeg",year:3,genre:"Peixe fresco da nossa costa",type:"Kilograms"},
      {name:"Carcaças", src:"../../assets/imgs/pao.jpg",year:10,genre:"Pão quentinho acabado de cozer",type:"Units"},
      {name:"Leite", src:"../../assets/imgs/leite.jpg",year:5,genre:"Leite meio gordo da mimosa",type:"Units"},
      {name:"Água", src:"../../assets/imgs/agua.jpg",year:2,genre:"Água fresca da montanha",type:"Liters"}
      ]
  } /* end of init function */

  delete(task: string){
      let index = this.tasks.indexOf(task);

      if(index > -1){
        this.tasks.splice(index,1);
      }
  }

  add(task: string, year:number, genre:string, select:string, path:string){
      this.tasks.push({name:task,src:path, year:year,genre:genre,type:select});
  }


}/* end TaskProvider class */
